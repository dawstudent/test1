DROP DATABASE IF EXISTS cinemaclub;
CREATE DATABASE cinemaclub;
USE cinemaclub;

CREATE TABLE actor (
    id_actor INT AUTO_INCREMENT,
    nom VARCHAR(20) NOT NULL,
    cognoms VARCHAR(40) NOT NULL,
    genere ENUM('H', 'D') NOT NULL,
    PRIMARY KEY (id_actor),
    CONSTRAINT uk_actor_nom_cognoms UNIQUE (nom, cognoms)
)  ENGINE=INNODB;

CREATE TABLE director (
    id_director INT AUTO_INCREMENT,
    nom VARCHAR(20) NOT NULL,
    cognoms VARCHAR(40) NOT NULL,
    PRIMARY KEY (id_director),
    CONSTRAINT uk_director_nom_cognoms UNIQUE (nom, cognoms)
)  ENGINE=INNODB;

CREATE TABLE generecinematografic (
    id_generecinematografic INT AUTO_INCREMENT,
    nom VARCHAR(30) NOT NULL,
    PRIMARY KEY (id_generecinematografic)
)  ENGINE=INNODB;

CREATE TABLE critic (
    id_critic INT AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_critic)
)  ENGINE=INNODB;

CREATE TABLE planta (
id_planta	TINYINT AUTO_INCREMENT,
numero		TINYINT NOT NULL,
PRIMARY KEY(id_planta)
)ENGINE = InnoDB;

CREATE TABLE sala (
id_sala		TINYINT AUTO_INCREMENT,
numero		TINYINT(2) NOT NULL,
tipus		ENUM('4K','8K','3D') NOT NULL,
planta_id	TINYINT,
PRIMARY KEY(id_sala, planta_id),
CONSTRAINT fk_sala_planta 
	FOREIGN KEY (planta_id) REFERENCES planta (id_planta)
)ENGINE = InnoDB;


CREATE TABLE pelicula (
    id_pelicula INT AUTO_INCREMENT,
    titol VARCHAR(50) NOT NULL,
    any SMALLINT(4),
    durada SMALLINT(3),
    idioma VARCHAR(20),
    pais VARCHAR(3),
    estrena DATETIME NULL,
    sala_id	TINYINT,
    planta_id TINYINT,
    PRIMARY KEY (id_pelicula),
    CONSTRAINT fk_pelicula_sala FOREIGN KEY (sala_id) REFERENCES sala (id_sala),
    CONSTRAINT fk_pelicula_planta FOREIGN KEY (planta_id) REFERENCES planta (id_planta)
)  ENGINE=INNODB;

CREATE TABLE actor_pelicula (
    id_actor INT,
    id_pelicula INT,
    rol VARCHAR(40),
    PRIMARY KEY (id_actor , id_pelicula),
    CONSTRAINT fk_actor_pelicula_actor FOREIGN KEY (id_actor)
        REFERENCES actor (id_actor),
    CONSTRAINT fk_actor_pelicula_pelicula FOREIGN KEY (id_pelicula)
        REFERENCES pelicula (id_pelicula)
)  ENGINE=INNODB;

CREATE TABLE generecinematografic_pelicula (
    id_generecinematografic INT,
    id_pelicula INT,
    PRIMARY KEY (id_generecinematografic, id_pelicula),
    CONSTRAINT fk_generecinematografic_pelicula_generecinematografic FOREIGN KEY (id_generecinematografic)
        REFERENCES generecinematografic (id_generecinematografic),
    CONSTRAINT fk_generecinematografic_pelicula_pelicula FOREIGN KEY (id_pelicula)
        REFERENCES pelicula (id_pelicula)
)  ENGINE=INNODB;

CREATE TABLE critic_pelicula (
    id_critic INT,
    id_pelicula INT,
    valoracio TINYINT(1) NOT NULL,
    PRIMARY KEY (id_critic, id_pelicula),
    CONSTRAINT fk_critic_pelicula_critic FOREIGN KEY (id_critic)
        REFERENCES critic (id_critic),
    CONSTRAINT fk_critic_pelicula_pelicula FOREIGN KEY (id_pelicula)
        REFERENCES pelicula (id_pelicula),
	CONSTRAINT ck_critic_pelicula_valoracio CHECK (valoracio BETWEEN 1 AND 10)
)  ENGINE=INNODB;

CREATE TABLE director_pelicula (
    id_director INT,
    id_pelicula INT,
    PRIMARY KEY (id_director, id_pelicula),
    CONSTRAINT fk_director_pelicula_director FOREIGN KEY (id_director)
        REFERENCES director (id_director),
    CONSTRAINT fk_director_pelicula_pelicula FOREIGN KEY (id_pelicula)
        REFERENCES pelicula (id_pelicula)
)  ENGINE=INNODB;

CREATE TABLE ticket (
id_ticket	INT AUTO_INCREMENT,
preu		TINYINT(2) 	NOT NULL,
tipus		ENUM('4K','8K','3D') NOT NULL,
PRIMARY KEY(id_ticket)
)ENGINE=INNODB;

CREATE table empleat (
	id_empleat		SMALLINT AUTO_INCREMENT,
	nom				VARCHAR(20) NOT NULL,
	cognoms			VARCHAR(40) NOT NULL,
	dni				VARCHAR(9) NOT NULL,
	data_naixement	DATE NOT NULL,
	telefon			VARCHAR(9) NOT NULL,
	correu			VARCHAR(40) NOT NULL,
	supervisor_id	SMALLINT,
	PRIMARY KEY(id_empleat),
	CONSTRAINT uk_empleat_nom_cognoms 
		UNIQUE (nom,cognoms),
	CONSTRAINT fk_empleat_empleat
		FOREIGN KEY (supervisor_id) REFERENCES empleat(id_empleat)
)	ENGINE = InnoDB;


CREATE TABLE client (
id_client	SMALLINT AUTO_INCREMENT,
nom			VARCHAR(15) NOT NULL,
cognoms		VARCHAR(30) NOT NULL,
dni			VARCHAR(9) NOT NULL,
telefon		VARCHAR(9) NOT NULL,
correu		VARCHAR(30),
data_naixement DATE NOT NULL,
tipus		ENUM('NO SOCI','SOCI', 'VIP'),
ticket_id 	INT,
empleat_id	SMALLINT,
pelicula_id INT,
PRIMARY KEY(id_client),
CONSTRAINT uk_client_nom_cognoms 
	UNIQUE (nom,cognoms),
CONSTRAINT fk_client_ticket
	FOREIGN KEY (ticket_id) REFERENCES ticket (id_ticket),
CONSTRAINT fk_client_empleat FOREIGN KEY (empleat_id) REFERENCES empleat (id_empleat),
CONSTRAINT fk_client_pelicula FOREIGN KEY (pelicula_id) REFERENCES pelicula (id_pelicula)
)ENGINE = InnoDB;

CREATE TABLE nsoci(
id_client	SMALLINT,
descompte	SMALLINT DEFAULT 0,
PRIMARY KEY (id_client),
CONSTRAINT fk_nsoci_client
	FOREIGN KEY (id_client) REFERENCES client (id_client)
)	ENGINE=INNODB;

CREATE TABLE soci(
id_client	SMALLINT,
descompte	SMALLINT DEFAULT 50,
PRIMARY KEY (id_client),
CONSTRAINT fk_soci_client
	FOREIGN KEY (id_client) REFERENCES client (id_client)
)ENGINE=INNODB;

CREATE TABLE vip(
id_client	SMALLINT,
descompte	SMALLINT DEFAULT 80,
PRIMARY KEY (id_client),
CONSTRAINT fk_vip_client
	FOREIGN KEY (id_client) REFERENCES client (id_client)
)ENGINE=INNODB;




