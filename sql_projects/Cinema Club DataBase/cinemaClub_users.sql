USE cinemaclub;
-- Creem el usuaris adients a la nostra base de dades (empleats)
CREATE USER IF NOT EXISTS 'isaac_colmenares' IDENTIFIED BY '1234';
CREATE USER IF NOT EXISTS 'alex_candela' IDENTIFIED BY '1234';
CREATE USER IF NOT EXISTS 'elias_garcia' IDENTIFIED BY '1234';
CREATE USER IF NOT EXISTS 'emilio_fernandez' IDENTIFIED BY '1234';
CREATE USER IF NOT EXISTS 'nil_arilla' IDENTIFIED BY '1234';
CREATE USER IF NOT EXISTS 'john_doe' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'jim_smith' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'sarah_johnson' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'emily_davis' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'matthew_wilson' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'ashley_johnson' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'jacob_lewis' IDENTIFIED BY '1234'; 
CREATE USER IF NOT EXISTS 'olivia_clark' IDENTIFIED BY '1234'; 

/*La nostra base de dades s’encarrega de recopilar la informació d’un cinema club.
Primer de tot hem generat el superadministrador de totes les base de dades i 
el superadministrador de la base de dades ‘cinemaclub’ anomenat ‘cc_admin’.*/

-- Creem el superadministrador de totes les base de dades:
GRANT ALL PRIVILEGES
ON *.*
TO superadmin IDENTIFIED BY '1234'
WITH GRANT OPTION;
FLUSH PRIVILEGES;

-- Creem el superadministrador de la base de dades cinemaclub
GRANT ALL PRIVILEGES
ON cinemaclub.*
TO 'cc_admin' IDENTIFIED BY '1234'
WITH GRANT OPTION;
FLUSH PRIVILEGES;

/*	
	La creació de nous usuaris i assignació de rols d’aquests només es realitza dins l'usuari cc-admin.
	De la mateixa manera, la modificació de la base de dades tant en la creació com en eliminació de taules. 
	En cas de realitzar un canvi gran, es creen contes per a l'equip extern contractat de programadors,
	atorgant als usuaris permisos específics centrats en la necessitat requerides.
*/
-- Volem crear l'usuari programer que pugui crear i eliminar taules en la database amb password 1234 .
GRANT CREATE, DROP
ON cinemaclub.*
TO programer IDENTIFIED BY '1234';
SHOW GRANTS FOR programer;
-- Si volem que ara només tinguin un permís d'ALTER, se'ls atorga de la següent manera.
REVOKE CREATE, DROP
ON cinemaclub.*
FROM programer;
GRANT ALTER
ON cinemaclub.*
TO programer;
FLUSH PRIVILEGES;
-- Un cop finalitzat el treball, s’eliminen les contes.
DROP USER programer;

/*
	La base de dades és divideix en 2 seccions. El manteniment del catàleg (pel·lícules) del cinema club 
    i en la gestió de clients i empresarial amb el responsable corresponent.
    Dins del manteniment del catàleg, tenim quatre rols:
*/
-- Catàleg de pel·lícules
	-- Rol d'incorporacio de noves pelicules
    /*
		Nil Arilla és el nostre empleat encarrgat de incorporar i/o actualitzar les novetats cinematográfiques al catàleg.
		Té permisos necessaris per veure, inserir i actualitzar les dades relacionedes amb informació de les pel·lícules, per exemple, 
		actor, director ...
    */
CREATE ROLE IF NOT EXISTS afegir_pelicula;
GRANT 
	SELECT(id_pelicula,titol,any,durada,idioma,pais,estrena), 
	INSERT(id_pelicula,titol,any,durada,idioma,pais,estrena), 
    UPDATE(id_pelicula,titol,any,durada,idioma,pais,estrena)
ON cinemaclub.pelicula
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.actor
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.director
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.actor_pelicula
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.director_pelicula
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.generecinematografic
TO afegir_pelicula;
GRANT SELECT, INSERT, UPDATE
ON cinemaclub.generecinematografic_pelicula
TO afegir_pelicula;
FLUSH PRIVILEGES;
GRANT afegir_pelicula TO 'nil_arilla';
FLUSH PRIVILEGES;
SET DEFAULT ROLE afegir_pelicula FOR 'nil_arilla';
-- Ho comprovem 
SHOW GRANTS FOR 'nil_arilla';
SHOW GRANTS FOR afegir_pelicula;
	-- Rold d'eliminacio de velles pel·lícules (SELECT, DELETE)
    /*
		Elias García és el nostre empleat encarrgat de eliminar pel·lícules poc beneficioses del catàleg.
		Té permisos necessaris per veure i eliminar les dades relacionedes amb informació de les pel·lícules, per exemple, 
		actor, director ...
    */
CREATE ROLE IF NOT EXISTS eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.pelicula
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.actor
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.director
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.actor_pelicula
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.director_pelicula
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.generecinematografic
TO eliminar_pelicula;
GRANT SELECT, DELETE
ON cinemaclub.generecinematografic_pelicula
TO eliminar_pelicula;
FLUSH PRIVILEGES;
GRANT eliminar_pelicula TO 'elias_garcia';
FLUSH PRIVILEGES;
SET DEFAULT ROLE eliminar_pelicula FOR 'elias_garcia';
SHOW GRANTS FOR 'elias_garcia';
	-- Rol d'administrador de valoracions
	/*
		Emilio Fernandez és el nostre empleat encarrgat d'assignar valoracions de la critica a les pel·lícules.
		Té permisos necessaris per veure, inserir, actualitzar i eliminar les dades relacionedes tant del critic i la valoració amb les
		pel·lícules, d'aquestes nomès té permis de select de l'id i titol.
	*/
CREATE ROLE IF NOT EXISTS valorar_pelicula;
GRANT SELECT (id_pelicula,titol)
ON cinemaclub.pelicula
TO valorar_pelicula;
GRANT SELECT, INSERT, UPDATE, DELETE
ON cinemaclub.critic
TO valorar_pelicula;
GRANT SELECT, INSERT, UPDATE, DELETE
ON cinemaclub.critic_pelicula
TO valorar_pelicula;
FLUSH PRIVILEGES;
GRANT valorar_pelicula TO 'emilio_fernandez';
FLUSH PRIVILEGES;
SET DEFAULT ROLE valorar_pelicula FOR 'emilio_fernandez';
SHOW GRANTS FOR 'emilio_fernandez';

-- Rol cap de manteniment de catàleg (Alex Candela)
	/*
		Alex Candela és co-fundador i màxim supervisor encarrgat de la secció de manteniment del catàleg.
		A part de tenir els rols encarregats del manteniment del cataleg (valorar_pelicula, afegir_pelicula, eliminar_pelicula),
		té control absolut en selecionar i modificar les taules de la seva regió.
	*/
CREATE ROLE IF NOT EXISTS cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.pelicula
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.director
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.actor
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.generecinematografic
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.critic
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.actor_pelicula
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.generecinematografic_pelicula
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.critic_pelicula
TO cap_cataleg;
GRANT SELECT,ALTER
ON cinemaclub.director_pelicula
TO cap_cataleg;
FLUSH PRIVILEGES;
GRANT cap_cataleg TO 'alex_candela';
GRANT afegir_pelicula TO 'alex_candela';
GRANT eliminar_pelicula TO 'alex_candela';
GRANT valorar_pelicula TO 'alex_candela';
FLUSH PRIVILEGES;
SET DEFAULT ROLE cap_cataleg FOR 'alex_candela';
SHOW GRANTS FOR 'alex_candela';
	-- Entrem en la conta de alex_candela:
SELECT * FROM information_schema.applicable_roles;
	-- Observem els rols que té assignats: afegir_pelicula, valorar_pelicula, eliminar_pelicula, cap_cataleg(aquest per defecte)
SELECT CURRENT_ROLE;
	-- Veiem que té activat el rol cap_cataleg

-- Dins de la gestió de clients i empresarial, tenim cinc rols:
	-- Rol de gestió de dades dels clients
	/*
		Els empleats John Doe i Jim Smith són els encarregats de gestionar les dades dels clients.
		Tenen permisos necessaris per veure, inserir, actualitzar i eliminar dades personals relacionades amb els clients
	*/
CREATE ROLE IF NOT EXISTS gestionar_client;
GRANT 
	SELECT(id_client, nom, cognoms, dni, telefon, correu, data_naixement, tipus),
    INSERT(id_client, nom, cognoms, dni, telefon, correu, data_naixement, tipus),
    UPDATE(id_client, nom, cognoms, dni, telefon, correu, data_naixement, tipus),
    DELETE
ON cinemaclub.client
TO gestionar_client;
GRANT SELECT,INSERT(id_client),DELETE
ON cinemaclub.soci
TO gestionar_client;
GRANT SELECT,INSERT(id_client),DELETE
ON cinemaclub.nsoci
TO gestionar_client;
GRANT SELECT,INSERT(id_client),DELETE
ON cinemaclub.vip
TO gestionar_client;
FLUSH PRIVILEGES;
GRANT gestionar_client TO 'john_doe','jim_smith';
FLUSH PRIVILEGES;
SET DEFAULT ROLE gestionar_client FOR 'john_doe';
SET DEFAULT ROLE gestionar_client FOR 'jim_smith';

	-- Rol de gestió del servei
	/*
		Els empleats Jacob Lewis i Olivia Clark tenen la funció de gestionar els serveis, que inclou l'assignació de sala i la planta
		on es farà projecció de la pel·licula i el ticket amb el client.
		Tenen permisos necessaris per veure les dades amb les que es relacioanran amb la pelicula i els client, d'aquest dos tenen permisos de update i select de informacio rellevant.
        Principalment aquesta funció la realizarà un sofware de forma autòmatica amb l'ajut d'una interficie atractiva a l'empleat.
        El cinema club té 4 empleats de taquiller, amb acces a aquest permís. John Doe i Jim Smith també tenen aquest rol peró no es el seu rol principal.
	*/
CREATE ROLE IF NOT EXISTS gestionar_servei;
GRANT SELECT
ON cinemaclub.ticket
TO gestionar_servei;
GRANT SELECT
ON cinemaclub.empleat
TO gestionar_servei;
GRANT SELECT
ON cinemaclub.sala
TO gestionar_servei;
GRANT SELECT
ON cinemaclub.planta
TO gestionar_servei;
GRANT SELECT(id_pelicula), UPDATE(sala_id,planta_id)
ON cinemaclub.pelicula
TO gestionar_servei;
GRANT SELECT(id_client, dni), UPDATE(ticket_id,empleat_id,pelicula_id)
ON cinemaclub.client
TO gestionar_servei;
FLUSH PRIVILEGES;
GRANT gestionar_servei TO 'jacob_lewis','olivia_clark';
GRANT gestionar_servei TO 'john_doe','jim_smith';
FLUSH PRIVILEGES;
SET DEFAULT ROLE gestionar_servei FOR 'jacob_lewis';
SET DEFAULT ROLE gestionar_servei FOR 'olivia_clark';
	-- Rol de recursos humans
	/*
		Els empleats Emily Davis i Matthew Wilson són els encarregats del departament de RRHH. 
		Tenen permisos per veure, inserir, actualitzar, eliminar les dades de la taula empleat.
	*/
CREATE ROLE IF NOT EXISTS recursos_humans;
GRANT SELECT, INSERT, UPDATE, DELETE
ON cinemaclub.empleat
TO recursos_humans;
FLUSH PRIVILEGES;
GRANT recursos_humans TO 'emily_davis','matthew_wilson';
FLUSH PRIVILEGES;
SET DEFAULT ROLE recursos_humans FOR 'emily_davis';
SET DEFAULT ROLE recursos_humans FOR 'matthew_wilson';
	-- Rol de finances 
	/*
		Les empleades Sarah Johnson i Ashley Johnson tenen permisos per modificar els tickets i descomptes
		i veure dades rellevants per fer un estudi dels clients i pelis més populars.
	*/
CREATE ROLE IF NOT EXISTS finances;
GRANT SELECT, INSERT, UPDATE, DELETE
ON cinemaclub.ticket
TO finances;
GRANT SELECT(id_client, nom, data_naixement, tipus, ticket_id, pelicula_id)
ON cinemaclub.client
TO finances;
GRANT SELECT
ON cinemaclub.pelicula
TO finances;
GRANT ALTER
ON cinemaclub.soci
TO finances;
GRANT ALTER
ON cinemaclub.nsoci
TO finances;
GRANT ALTER
ON cinemaclub.vip
TO finances;
FLUSH PRIVILEGES;
GRANT finances TO 'sarah_johnson','ashley_johnson';
SET DEFAULT ROLE finances FOR 'sarah_johnson';
SET DEFAULT ROLE finances FOR 'ashley_johnson';
	-- Rol de cap de gestió (Isaac Colmenares)
	/*
		Isaac Colmenares és co-fundador i màxim supervisor encarrgat de la secció de gestió de ventes.
		A part de tenir els rols encarregats de gestio de ventes (gestionar_client, gestionar_client, recursos_humans, finances),
		té control absolut en modificar les taules de la seva regió excepte les referents a la generalització de clients d'ús exclusiu 
        al rol de finances.
	*/
CREATE ROLE IF NOT EXISTS cap_gestio;
GRANT SELECT,ALTER
ON cinemaclub.planta
TO cap_gestio;
GRANT SELECT,ALTER
ON cinemaclub.sala
TO cap_gestio;
GRANT SELECT,ALTER
ON cinemaclub.ticket
TO cap_gestio;
GRANT SELECT,ALTER
ON cinemaclub.client
TO cap_gestio;
GRANT SELECT,ALTER
ON cinemaclub.empleat
TO cap_gestio;
FLUSH PRIVILEGES;
GRANT cap_gestio TO 'isaac_colmenares';
GRANT gestionar_client TO 'isaac_colmenares';
GRANT gestionar_servei TO 'isaac_colmenares';
GRANT recursos_humans TO 'isaac_colmenares';
GRANT finances TO 'isaac_colmenares';
FLUSH PRIVILEGES;
SET DEFAULT ROLE cap_gestio FOR 'isaac_colmenares';
